//1931517
//Estefan Maheux-Saban
package Question3;

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

//Ui of the game
public class DiceApplication extends Application{
	

	public void start(Stage stage) {
		Group root = new Group();
		//containers
		HBox hboxChoice = new HBox();
		HBox hboxInfo = new HBox();
		VBox ui = new VBox();
		
		//Ui elements
		TextField amount = new TextField();
		Button btnOdd = new Button("Odd");
		Button btnEven = new Button("Even");
		
		Label message = new Label("Insert amount to bet");
		Label funding = new Label("	Fund: 250");
		//object that handles the game
		DiceGame game = new DiceGame();
		//events in function of the chosen button clicked
		DiceChoice odd = new DiceChoice(message, funding, amount, "odd", game);
		DiceChoice even = new DiceChoice(message, funding, amount, "even", game);
		
		btnOdd.setOnAction(odd);
		btnEven.setOnAction(even);
		
		//add container to the scene
		hboxInfo.getChildren().addAll(message, funding);
		hboxChoice.getChildren().addAll(amount, btnOdd, btnEven);
		ui.getChildren().addAll(hboxInfo, hboxChoice);
		root.getChildren().add(ui);
		
		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.WHITE);
		
		//associate scene to stage and show
		stage.setTitle("Dice!"); 
		stage.setScene(scene); 
		
		stage.show(); 
	}
	//starts the program
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		 Application.launch(args);
	}

}
