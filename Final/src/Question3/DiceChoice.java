//1931517
//Estefan Maheux-Saban
package Question3;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.*;

//Interaction between the UI and the game
public class DiceChoice implements EventHandler<ActionEvent>{
	private Label message;
	private Label funding;
	private TextField amount;
	private String choice;
	private DiceGame game;
	
	//constructor
	public DiceChoice(Label message, Label funding, TextField amount,String choice, DiceGame game) {
		this.message = message;
		this.funding = funding;
		this.amount = amount;
		this.choice = choice;
		this.game = game;
	}
	
	
	
	//handles the event with the odd and even button
	@Override
	public void handle(ActionEvent e) {
		String result = game.playBet(Integer.parseInt(amount.getText()), choice);
		message.setText(result);
		funding.setText("  Fund :" + game.getFund());
	}
}
