//1931517
//Estefan Maheux-Saban
package Question3;

import java.util.Random;

//Logic of the game
public class DiceGame {
	private int fund;
	private Random rng;
	
	//constructor
	public DiceGame() {
		this.fund = 250;
		rng = new Random();
	}
	
	//Plays the bet with the amount enter and the choice of the play on even or odd
	public String playBet(int bet, String choice) {
		int dice = roll();
		if(this.fund >= bet) {
			if(choice.equals("odd")) {
				if(dice % 2 == 1 ) {
					this.fund += bet;
					return "Dice landed on " + dice + " You won your bet!";
				}
				else {
					this.fund -= bet;
					return "Dice landed on " + dice + " You lost your bet!";
				}
			}
			if(choice.equals("even")) {
				if(dice % 2 == 0 ) {
					this.fund += bet;
					return "Dice landed on " + dice + " You won your bet!";
				}
				else {
					this.fund -= bet;
					return "Dice landed on " + dice + " You lost your bet!";
				}
			}
		}
		return "Too much money bet!";
	}
	//roll the dice to get a result
	public int roll() {
		return this.rng.nextInt(6);
	}
	//returns the amount of money the user has
	public int getFund() {
		return this.fund;
	}
}
