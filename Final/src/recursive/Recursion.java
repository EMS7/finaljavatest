//1931517
//Estefan Maheux-Saban
package recursive;

public class Recursion {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] test = {0,2,4,6,8,9};
		System.out.println(recursiveCount(test, 1));

	}
	
	public static int recursiveCount(int[] numbers, int n) {
		if(numbers.length > 2) {
			int[] lessNumbers = new int[numbers.length - 2];
			for(int i = 0; i < lessNumbers.length; i++) {
				lessNumbers[i] = numbers[i+2];
			}
			if(numbers[0] < 10 && numbers[0] >= n) {
				return 1 + recursiveCount(lessNumbers, n);
			}
			return 0 + recursiveCount(lessNumbers, n);
		}
		return (numbers[0] < 10 && numbers[0] >= n ? 1 : 0);
	}

}
